package Server;

import java.io.Console;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.util.HashMap;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONObject;

public class RequestFromClient {
	private static JSONParser jsonParser = new JSONParser();

	public RequestFromClient(DataInputStream dataInputStream,DataOutputStream dataOutputStream) throws Exception {
		String clientRequest="";  
		String termination="";
		while(!termination.equals("stop"))
		{  
			clientRequest=dataInputStream.readUTF();  
		JSONObject	jsonObject=(JSONObject) jsonParser.parse(clientRequest);
			if(!jsonObject.get("command").equals("stop"))
			{
				try
				{
					if(jsonObject.get("command").equals("set"))
					{
						jsonObject.remove("command");
						boolean isCustomerAdded= new Operations().InsertData(Memory.hashMap, jsonObject);
						if(isCustomerAdded==true)
						{
							dataOutputStream.writeUTF("Ok");
				            dataOutputStream.flush();	
						}
						else
						{
							dataOutputStream.writeUTF("Updated"); 
				            dataOutputStream.flush();	
						}
					}
					else
					{
						String value=new Operations().GetValue(Memory.hashMap, jsonObject);
						dataOutputStream.writeUTF(value); 
						 dataOutputStream.flush();
						 
					}
			 }
				catch(Exception exception)
				{
					dataOutputStream.writeUTF("Invalid command"); 
					System.out.print(exception);
					dataOutputStream.flush();	 
				}
			}
			else
			{
				termination="stop";
				dataOutputStream.writeUTF("Terminated"); 
				 dataOutputStream.flush();		 
			} 
		}  
	}

}
