package Server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ThreadClass extends Thread{
	private Socket socket;
	private DataInputStream dataInputStream;
	private DataOutputStream dataOutputStream;
	private ServerSocket serverSocket;
	private Object object;
	
	public ThreadClass(Socket sock)
	{
		socket=sock;
	}
	public void run()
	{
		try{
			
			dataInputStream = new DataInputStream(socket.getInputStream());
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			new BufferedReader(new InputStreamReader(System.in));
			new RequestFromClient(dataInputStream, dataOutputStream);
			new CloseServerConnection().CloseConnection(dataInputStream, dataOutputStream, socket, serverSocket);
		}
		catch (Exception e) {
			System.out.println(e);
			
		}
	}
}