package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class CloseServerConnection {

	public void CloseConnection(DataInputStream dataInputStream,DataOutputStream dataOutputStream,Socket socket,ServerSocket serverSocket) throws IOException {
		dataOutputStream.close();
		dataInputStream.close();
		socket.close();
		serverSocket.close();
	}

}
