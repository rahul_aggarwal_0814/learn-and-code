package Server;

import java.net.*;
import java.util.HashMap;

import javax.naming.ldap.StartTlsRequest;

import java.io.*;

public class Server {
	private Socket socket;
	private ServerSocket serverSocket;
	private Object object;

	public static void main(String args[]) throws Exception {
		new Server().setup();
	}


	
	public void setup() throws Exception
	{
		serverSocket = new ServerSocket(9996);
		System.out.println("Wait for Client To get Connected");
		while(true)
		{
		socket = serverSocket.accept();
		System.out.println("Client is connected");
		ThreadClass serverThread= new ThreadClass(socket);
		serverThread.start();
		}
	}
}
