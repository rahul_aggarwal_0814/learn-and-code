package Server;
import java.net.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.parser.JSONParser;

public class Operations {

	public boolean InsertData(HashMap<String, String> hashMap,JSONObject jsonObject) {
		boolean isCustomerAdded=false;
		
		boolean val=hashMap.containsKey(jsonObject.get("key"));
		
        if(val==true)
        {
        	UpdateValue(hashMap, jsonObject);
        }
        else
        {
			   
        	String KeyData=(String) jsonObject.get("key");
        	String ValueData=(String) jsonObject.get("value");
        	String timedata=(String)jsonObject.get("time");   
        	int time=Integer.parseInt(timedata);
        	hashMap.put(KeyData, ValueData);
        	isCustomerAdded=true;
        	Timer timer = new Timer(); 
    		timer.schedule(new helper(hashMap,jsonObject), time*1000);
        }
        
        return isCustomerAdded;
	}
	
	public void UpdateValue(HashMap<String, String> hashMap,JSONObject jsonObject)
	{
		String KeyData=(String) jsonObject.get("key");
    	String ValueData=(String) jsonObject.get("value");
    	String timedata=(String)jsonObject.get("time");   
    	int time=Integer.parseInt(timedata);
		hashMap.replace(KeyData, ValueData);
		Timer timer = new Timer(); 
		timer.schedule(new helper(hashMap,jsonObject), time*1000);
	}
	
	public String GetValue(HashMap<String, String> hashMap,JSONObject jsonObject) {
		String value = "";
		boolean val=hashMap.containsKey(jsonObject.get("key"));
		if(val==true)
        {
        	value=hashMap.get(jsonObject.get("key"));
        }
        else
        {
			value="Key not found";
		}  
		
        return value;
	}

}
class helper extends TimerTask{
	HashMap<String, String> hashMap;
	JSONObject jsonObject;
	
	public  helper(HashMap<String, String> hashMap,JSONObject jsonObject) {
		this.hashMap=hashMap;
		 this.jsonObject=jsonObject;
	}
	public void run() 
    { 
		String KeyData=(String) jsonObject.get("key");
    	hashMap.remove(KeyData);
        
    } 
}
