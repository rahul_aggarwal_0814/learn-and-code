package Client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CloseClientConnection {

	public void CloseConnection(DataInputStream dataInputStream,DataOutputStream dataOutputStream,Socket socket) throws IOException {
		dataInputStream.close();
		dataOutputStream.close();  
		socket.close();  
	}

}
