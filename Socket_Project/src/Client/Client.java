package Client;

import java.net.*;
import java.io.*;
import org.json.JSONObject;

class Client {
	static Socket socket;
	static DataInputStream dataInputStream;
	static DataOutputStream dataOutputStream;
	static BufferedReader buffredReader;
	static String command = "";

	public static void main(String args[]) throws Exception {
		try {
			socket = new Socket("localhost", 9996);

			System.out.println("Client is connected");
			setup();
			new RequestToServer(dataInputStream, dataOutputStream, buffredReader);
			new CloseClientConnection().CloseConnection(dataInputStream, dataOutputStream, socket);
		} catch (SocketException socktException) {
			System.out.println("Server is not Connected");
		}
	}

	public static void setup() throws Exception {
		dataInputStream = new DataInputStream(socket.getInputStream());
		dataOutputStream = new DataOutputStream(socket.getOutputStream());
		buffredReader = new BufferedReader(new InputStreamReader(System.in));

	}

	
}