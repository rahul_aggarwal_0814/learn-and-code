package Client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.util.HashMap;
import org.json.simple.parser.JSONParser;

public class RequestToServer {
	static String command="";
	public RequestToServer(DataInputStream dataInputStream,DataOutputStream dataOutputStream,BufferedReader buffredReader) throws IOException
	{
		String inputFromUSer="";
		JSONObject json=new JSONObject();
		while(!command.equals("stop"))
		{  
			
	                
	                    	inputFromUSer=buffredReader.readLine();
	                    	String[] arrOfStr = inputFromUSer.split(" ", 4); 
	                    	if(arrOfStr[0].equals("set"))
	                    	{
	                    		json.put("command", arrOfStr[0]);
		            			json.put("key", arrOfStr[1]);
		            			json.put("value", arrOfStr[2]);
		            			json.put("time", arrOfStr[3]);
	                    	}
	                    	else if(arrOfStr[0].equals("get"))
	                    	{
	                    		json.put("command", arrOfStr[0]);
		            			json.put("key", arrOfStr[1]);
	                    	}
	                    	else{
	                    		json.put("command", "stop");
	                    		command="stop";
	                    		
	                    	}
	            			dataOutputStream.writeUTF(json.toString());  
	            			dataOutputStream.flush();
	            			new ResponseFromServer(dataInputStream);
	            
		} 
	}

}
