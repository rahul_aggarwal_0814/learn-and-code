import random
def rolledDiceFunction(maximumNo):
    diceResult=random.randint(1, maximumNo)
    return diceResult


def main():
    maximumNo=6
    isDiceRolled=True
    while isDiceRolled:
        userInput=input("Ready to roll? Enter Q to Quit")
        if userInput.lower() !="q":
            diceResult=rolledDiceFunction(maximumNo)
            print("You have rolled a",diceResult)
        else:
            isDiceRolled=False
